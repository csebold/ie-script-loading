(function() {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = loadScript;
    httpRequest.open('GET', 'https://csebold.gitlab.io/ie-script-loading/secondary.js', true);
    httpRequest.send();

    function loadScript() {
        if(httpRequest.readyState === XMLHttpRequest.DONE) {
            if(httpRequest.status === 200) {
                var ref = document.getElementsByTagName('script')[0];
                var scr = document.createElement('script');
                scr.text = httpRequest.responseText;
                //document.write('<script>'+scr.text+'</script>');
                ref.parentNode.insertBefore(scr, ref);
                //eval(httpRequest.responseText);
            } else { alert('Non error 200'); }
        }
    }
})();